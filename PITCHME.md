# DNSSEC

---

# DNS

---
### Zone

Séparé par des points

---

![](https://www.cloudflare.com/img/learning/dns/dns-server-types/recursive-resolver.png)

---

## Types d’enregistrement

- A, AAAA : IPv4/v6 adresse
- MX : seveur mail
- NS : délégation de zone

---

![](https://www.cloudflare.com/img/products/ssl/diagram-rrsets.svg)

---
### Spoofing 1/2

![](https://www.cloudflare.com/img/learning/dns/dns-cache-poisoning/dns-cache-poisoning-attack.svg)

---
### Spoofing 2/2

![](https://www.cloudflare.com/img/learning/dns/dns-cache-poisoning/dns-cache-poisoned.svg)

---

## DNSSEC

---

### Enregistrement


+ RRSIG : signature crypto
+ DNSKEY : clé publique pour signature
+ DS : hash dnskey
+ NSEC and NSEC3 : nier explicitement
+ CDNSKEY and CDS : délégation de zone

---

### Signature

![](https://www.cloudflare.com/img/products/ssl/diagram-rrsets.svg)

---

![](https://www.cloudflare.com/img/products/ssl/diagram-zone-signing-keys-2.svg)

---

---
#### CDS

![](https://www.cloudflare.com/img/products/ssl/diagram-delegation-signer-records.svg)

---

![](https://www.cloudflare.com/img/products/ssl/diagram-the-chain-of-trust.svg)

---

### Rotation clé

![](https://www.cloudflare.com/img/products/ssl/diagram-key-signing-keys-1.svg)


---

### Nier existence

- NSEC next secure domain
- NSEC3 hash du next secure domain

---

### Biblio

[Cloud Flare](https://www.cloudflare.com/dns/dnssec/how-dnssec-works/)
